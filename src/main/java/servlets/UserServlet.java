package servlets;


import dao.UserDao;
import dao.impl.jdbc.UserDaoJDBCImpl;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/user/*")
public class UserServlet extends HttpServlet {
    private static final String METHOD_CREATE = "create";
    private static final String METHOD_LIST = "list";
    UserDao userDao;

    @Override
    public void init() throws ServletException {
        userDao = new UserDaoJDBCImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getPathInfo().substring(1);

        if (METHOD_CREATE.equals(action)) {

        } else if (METHOD_LIST.equals(action)) {

        } else {
            getUserById(request, response, action);
        }
    }

    private void getUserById(HttpServletRequest request, HttpServletResponse response, String action)
            throws ServletException, IOException {
        Long id;
        try {
            id = Long.valueOf(action);
        } catch (NumberFormatException e) {
            id = 1L;
        }

        User user = userDao.getUserById(id);
        request.setAttribute("user", user);

        request.getRequestDispatcher("/userProfile.ftl").forward(request, response);
    }
}
