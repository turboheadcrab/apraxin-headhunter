package servlets;

import dao.CVDao;
import dao.impl.hibernate.CVDaoHibernateImpl;
import dao.impl.jdbc.CVDaoJDBCImpl;
import model.CV;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/cv")
public class CVServlet extends HttpServlet {

    private static final String GET_BY_ID = "get";
    private static final String GET_LIST = "list";
    private static final String CREATE = "create";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String flag = request.getParameter("action");
        if (flag.equals(GET_BY_ID)) {
            getCVById(request, response);
        } else if (flag.equals(GET_LIST)) {
            getCVList(request, response);
        } else if (flag.equals(CREATE)) {
            redirectCvCreatePage(request, response);
        } else {
            response.sendRedirect("/");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String text = request.getParameter("text");
        String title = request.getParameter("title");
        String education = request.getParameter("education");
        String experience = request.getParameter("experince");
        Long userId = Long.parseLong(request.getParameter("user"));

        User newUser = new User();
        newUser.setId(userId);

        CV newCV = new CV();
        newCV.setTitle(title);
        newCV.setText(text);
        newCV.setEducation(education);
        newCV.setExperience(experience);
        newCV.setUser(newUser);

        CVDao cvDao = new CVDaoHibernateImpl();
        cvDao.add(newCV);
        request.setAttribute("cv", newCV);
        request.getRequestDispatcher("/").forward(request, response);
    }

    private void redirectCvCreatePage(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/cv/create.ftl").forward(request, response);
    }

    private CV mapRequestToCV(HttpServletRequest req) {
        Long userId = Long.valueOf(req.getParameter("user"));
        User user = new User();
        user.setId(userId);
        CV newCV = new CV();
        newCV.setTitle(req.getParameter("title"));
        newCV.setText(req.getParameter("text"));
        newCV.setEducation(req.getParameter("education"));
        newCV.setExperience(req.getParameter("experience"));
        return newCV;
    }

    private void getCVList(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CVDao cvDao = new CVDaoJDBCImpl();
        List<CV> cvList = cvDao.cvList();

        request.setAttribute("cvlist", cvList);
        getServletContext().getRequestDispatcher("/cv/list.ftl").forward(request, response);
    }

    private void getCVById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CVDao cvDao = new CVDaoJDBCImpl();

        String id = request.getParameter("id");
        if (id == null) {
            response.sendRedirect("/");
            return;
        }
        CV cv = cvDao.getCvById(Long.valueOf(id));
        request.setAttribute("cv", cv);
        getServletContext().getRequestDispatcher("/cv/page.ftl").forward(request, response);
    }
}
